// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var bower = require('gulp-bower');
var connect = require('gulp-connect-multi')();

var gulpssh = require('gulp-ssh');


gulp.task('connect', connect.server({
  root: ['app'],
  port: 1337,
  livereload: true,
  open: {
    browser: 'chrome' // if not working OS X browser: 'Google Chrome'
  }
}));

gulp.task('html', function() {
  gulp.src('/app/*.html')
    .pipe(connect.reload());
  gulp.src('/app/*.html')
    .pipe(connect.reload());
});

// Lint Task
gulp.task('lint', function() {
  return gulp.src('app/js/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

//Bower task
gulp.task('bower', function() {
  return bower();
});

// Compile Our Sass
gulp.task('sass', function() {
  return gulp.src('app/scss/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('css'));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
  return gulp.src('app/js/*.js')
    .pipe(concat('all.js'))
    .pipe(gulp.dest('dist'))
    .pipe(rename('all.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist'));
});

// Watch Files For Changes
gulp.task('watch', function() {
  gulp.watch('app/js/*.js', ['lint']);
  gulp.watch('app/*.html', ['html']);
  gulp.watch('app/scss/*.scss', ['sass']);
});

// Default Task

gulp.task('default', ['lint', 'sass', 'connect', 'watch']);
