'use strict';

angular.module('agile.family', ['ui.router', 'ngToast'])
  .config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/");

    var nav =  {
      templateUrl: "views/nav.html",
      controller: "NavCtrl"
    };

    $stateProvider
      .state('main', {
        url: "/",
        views: {
          'nav': nav,
          'main': {
            template: "<p>Välj en person ovan</p>"
          },
        }
      })
      .state('child', {
        url: "/child/:name",
        views: {
          'nav': nav,
          'main': {
            templateUrl: "views/child.html",
            controller: "ChildCtrl"
          }
        }
      })
      .state('admin', {
        url: "/admin",
        views: {
          'nav': nav,
          'main': {
            templateUrl: "views/admin.html",
            controller: "AdminCtrl"
          }
        }
      });
  });
