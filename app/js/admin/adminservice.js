angular.module('agile.family').service('AdminService', function adminService($q, $http) {
  var settings = {
    apiUrl: '/api',
  };

  var getAdminViewModel = function() {
    return $http.get(settings.apiUrl + '/admin')
    .then(function(response) {
      return response.data;
    });
  };

  var saveTask = function(task) {
    return $http.post(settings.apiUrl + '/task', task);
  };

  var saveReward = function(reward) {
    return $http.post(settings.apiUrl + '/reward', reward);
  };

  return {
    getAdminViewModel: getAdminViewModel,
    saveTask: saveTask,
    saveReward: saveReward
  };
});
