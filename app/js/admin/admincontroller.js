'use strict';

var rewardModel = function() {
  var self = this;
  self.id = null;
  self.title = '';
  self.description = '';
  self.editing = true;
};

var taskModel = function() {
  var self = this;
  self.id = null;
  self.title = '';
  self.howOften = '';
  self.minAge = 0;
  self.Whens = [];
  self.Rewards = [];
  self.editing = true;
};

angular.module('agile.family').controller('AdminCtrl', function($scope, $stateParams, ngToast, AdminService) {

  $scope.tasks = [];
  $scope.rewards = [];
  $scope.whens = [];

  $scope.howOften = {
    'daily': 'Dagligen',
    'weekday': 'Vardagar',
    'weekend': 'Helger',
    'monday': 'Måndagar',
    'tuesday': 'Tisdagar',
    'wednesday': 'Onsdagar',
    'thursday': 'Torsdagar',
    'friday': 'Fredagar',
    'saturday': 'Lördagar',
    'sunday': 'Söndagar'
  };

  AdminService.getAdminViewModel()
    .then(function(vm) {
      $scope.tasks = _.map(vm.tasks, function(task) {
        _.assign(task, {
          editing: false
        });
        return task;
      });
      $scope.rewards = _.map(vm.rewards, function(reward) {
        _.assign(reward, {
          editing: false
        });
        return reward;
      });

      $scope.whens = vm.whens;
    });

  $scope.toggleEdit = function(item) {
    item.editing = !item.editing;
  };

  $scope.saveTask = function(task) {
    //save the task!
    AdminService.saveTask(task)
      .then(function(response) {
        if (task.id === null) {
          task.id = response.data.id;
        }
        ngToast.create('Saved task ' + task.title);
        $scope.toggleEdit(task);
      });
  };

  $scope.saveReward = function(reward) {
    AdminService.saveReward(reward)
      .then(function(response) {
        if (reward.id === null) {
          reward.id = response.data.id;
        }
        ngToast.create('Sparat belöning ' + reward.title);
        $scope.toggleEdit(reward);
      });
  }

  $scope.taskWhenButtonClass = function(when, task) {
    return getButtonCssClass(taskHasWhen(when, task));
  };

  var taskHasWhen = function(when, task) {
    return _.some(task.Whens, function(taskWhen) {
      return taskWhen.id == when.id;
    });
  };

  var taskHasReward = function(reward, task) {
    return _.some(task.Rewards, function(taskReward) {
      return taskReward.id == reward.id;
    });
  };

  $scope.taskRewardButtonClass = function(reward, task) {
    return getButtonCssClass(taskHasReward(reward, task));
  };

  var getButtonCssClass = function(enabled) {
    var cssClass = "btn btn-raised btn-block";
    if (enabled) {
      cssClass += " btn-success";
    } else {
      cssClass += " btn-danger";
    }
    return cssClass;
  };

  $scope.toggleReward = function(reward, task) {
    //Need some api-function for this? No, just add the when to the Whens, then when (hihi)
    //save the task, it will work automagically?
    if (taskHasReward(reward, task)) {
      _.remove(task.Rewards, function(taskReward) {
        return taskReward.id == reward.id;
      });
    } else {
      task.Rewards.push(reward);
    }
  };

  $scope.toggleWhen = function(when, task) {
    //Need some api-function for this? No, just add the when to the Whens, then when (hihi)
    //save the task, it will work automagically?
    if (taskHasWhen(when, task)) {
      _.remove(task.Whens, function(taskWhen) {
        return taskWhen.id == when.id;
      });
    } else {
      task.Whens.push(when);
    }
  };

  $scope.newTask = function() {
    var newTask;
    newTask = _.find($scope.tasks, {
      id: null
    });
    if (typeof(newTask) === 'undefined') {
      newTask = new taskModel();
      $scope.tasks.push(newTask);
    }
    newTask.editing = true;
  };

  $scope.newReward = function() {
    var newReward;
    newReward = _.find($scope.rewards, {
      id: null
    });
    if (typeof(newReward) === 'undefined') {
      newReward = new rewardModel();
      $scope.rewards.push(newReward);
    }
    newReward.editing = true;
  };

});
