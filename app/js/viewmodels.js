var Reward = function(jsonData, tasks) {

  /*
  reduce / map all tasks from this object (in jsondata) to a task in the supplied
  task array. Add prototype function that can say if the reward is available (already there)
  copy necessary values? extend self?
  */
  this.tasks = _.filter(tasks, function(task) {
    return _.some(jsonData.Tasks, function(Task) {
      return Task.id == task.id;
    });
  });
  this.id = jsonData.id;
  this.title = jsonData.title;
};

Reward.prototype = {
  canCash: function() {
    if (this.tasks.length > 0) {
      return _.every(this.tasks, function(task) {
        return task.done === true;
      });
      return false;
    }
  }
};


(function() {

  var ChildViewModel = function(jsonData) {
    var internalModel = jsonData;
    this.child = jsonData.child; //needs model?
    this.tasks = jsonData.tasks;
    var rewards = [];
    _.forEach(jsonData.rewards, function(reward) {
      rewards.push(new Reward(reward, jsonData.tasks));
    });
    this.rewards = rewards;
  };

  ChildViewModel.prototype = {

  };

  var module = angular.module("agile.family");
  module.value("ChildViewModel", ChildViewModel);
}());
