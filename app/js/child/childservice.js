angular.module('agile.family').service('ChildService', function childService($q, $http, WhenService, modelTransformer, ChildViewModel) {
  var settings = {
    apiUrl: '/api',
    entityUrl: '/entities',
    childUrl: '/child',
    taskUrl: '/task',
    childTaskUrl: '/childtask'
  };

  var getChildren = function() {
    return $http.get(settings.apiUrl + '/children')
    .then(function(response) {
      return response.data;
    });
  };

  var getChildViewModel = function(name) {
    var when = WhenService.when();
    return $http.get(settings.apiUrl + settings.childUrl + '/name/' + name + '/' + when.when + '/' + when.typeOfDay + '/' + when.weekday)
    .then(function(response) {
      var childViewModel = modelTransformer.transform(response.data, ChildViewModel);
      return childViewModel;
    });
  };

// '/task/:taskId/:when/:childId/:done'
  var setTaskStatusForChild = function(childId, taskId, done) {
    var when = WhenService.timeOfDay();
    return $http.put(settings.apiUrl + settings.taskUrl + '/' + taskId + '/' + when + '/' + childId + '/' + done);
  };

  return {
    getChildren: getChildren,
    getChildViewModel: getChildViewModel,
    setTaskStatusForChild: setTaskStatusForChild
  };
});
