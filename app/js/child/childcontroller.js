'use strict';

angular.module('agile.family').controller('ChildCtrl', function($scope, $stateParams, ngToast, ChildService) {
  $scope.child = {};
  $scope.tasks = [];
  $scope.rewards = [];

  ChildService.getChildViewModel($stateParams.name)
  .then(function(viewModel) {
    $scope.child = viewModel.child;
    $scope.tasks = viewModel.tasks;
    $scope.rewards = viewModel.rewards;
  });

  $scope.taskClass = function(task) {
    if(task.done) {
      return "panel panel-info";
    }
    return "panel panel-primary";
  };

  $scope.changeTask = function(task) {
    task.done = !task.done;
    ChildService.setTaskStatusForChild($scope.child.id, task.id, task.done)
    .then(null, function(error) {
      console.log(error);
      ngToast.warning('Något fick fel när status skulle uppdateras');
    });
  };
});
