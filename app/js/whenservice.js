angular.module('agile.family').service('WhenService', function() {

  //1. What time is love?
  var when = function() {
    var now = moment();
    return {
      when: timeOfDay(now),
      weekday: now.format('dddd'),
      typeOfDay: isItAWeekDay(now) ? 'weekday' : 'weekend'
    };
  };

  var timeOfDay = function(now) {
    if (typeof(now) === 'undefined') {
      now = moment();
    }
    //Mellan fem och elva Ã¤r morgon
    if ((now.hour() >= 5 && now.hour() < 12)) {
      return 'morning';
    }
    if ((now.hour() >= 12 && now.hour() < 19)) {
      return 'afternoon';
    }
    if ((now.hour() >= 19 && now.hour() < 24)) {
      return 'evening';
    }
    return 'morning';
  };

  var isItAWeekDay = function(now) {
    // inte helg = vardag;
    return !(now.day() === 6 || now.day() === 0); //inte locale-aware, fÃ¶r jag orkar inte. 0 = sÃ¶ndag
  };

  return {
    when: when,
    timeOfDay: timeOfDay,
    isItAWeekDay: isItAWeekDay
  };
});
