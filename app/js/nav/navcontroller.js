angular.module('agile.family').controller('NavCtrl', function($scope, $stateParams, ngToast, ChildService) {
  $scope.children = [];
  ChildService.getChildren().then(function(children) {
    $scope.children = children;
  }, function(error) {
    console.log(error);
    ngToast.warning('Något gick fel vid inläsning av barn');
  });
});
