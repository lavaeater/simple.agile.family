'use strict'

var _ = require('lodash');
var Promise = require('bluebird');
var data = {rewards: {}};

var mapWhens = function(whenRows) {
  data.when = _.keyBy(whenRows, function(row) {
    return row.id;
  });
};

var createRewards = function(models) {
  //Do this before tasks - so we can associate rewards when we create tasks... or wut?
  //We should select them out and map them in a collection as we did with whens, that's pretty excellent
  return models.Reward.findOrCreate({where:{title: 'Rita med en förälder'}})
  .spread(function(reward, created) {
    data.rewards.draw = reward;
    return models.Reward.findOrCreate({where: { title: 'Bygga LEGO med en förälder'}});
  })
  .spread(function(reward, created) {
    //är det där ens ett promise? Återstår att se!
    data.rewards.lego = reward;
    return models.Reward.findOrCreate({where: {title:'Välja bok att läsa'}});
  })
  .spread(function(reward, created) {
    data.rewards.book = reward;
    return models.Reward.findOrCreate({where:{title:'Planera lekträff'}});
  }).spread(function(reward, created) {
    data.rewards.playdate = reward;
    return data.rewards; //Last return
  });

};

var createTasks = function(models) {
  return models.Task.findOrCreate({
      where: {
        title: 'Borsta tänderna',
        howOften: 'daily',
        minAge: 3
      }
    })
    .spread(function(task, created) {
      return task.setWhens([data.when.morning, data.when.evening]);
    })
    .then(function() {
      return models.Task.findOrCreate({
          where: {
            title: 'Ta fram kläder',
            howOften: 'daily',
            minAge: 5
          }
        })
        .spread(function(task, created) {
          return Promise.all(task.setWhens([data.when.evening]), task.setRewards([data.rewards.book, data.rewards.lego, data.rewards.draw]));
        });
    })
    .then(function() {
      return models.Task.findOrCreate({
          where: {
            title: 'Klä på sig',
            howOften: 'daily',
            minAge: 5
          }
        })
        .spread(function(task, created) {
          return Promise.all(task.setWhens([data.when.morning]), task.setRewards([data.rewards.book, data.rewards.lego, data.rewards.draw]));
        });
    })
    .then(function() {
      return models.Task.findOrCreate({
          where: {
            title: 'Städa rummet',
            howOften: 'weekend',
            minAge: 5
          }
        })
        .spread(function(task, created) {
          return Promise.all(task.setWhens([data.when.anytime]), task.setRewards([data.rewards.book, data.rewards.lego, data.rewards.draw, data.rewards.playdate]));
        });
    })
    .then(function() {
      return models.Task.findOrCreate({
          where: {
            title: 'Göra läxorna',
            howOften: 'weekday',
            minAge: 7
          }
        })
        .spread(function(task, created) {
          return Promise.all(task.setWhens([data.when.afternoon]), task.setRewards([data.rewards.book, data.rewards.lego, data.rewards.draw, data.rewards.playdate]));
        });
    })
    .then(function() {
      return models.Task.findOrCreate({
          where: {
            title: 'Träna som en Ninja Warrior',
            howOften: 'weekday',
            minAge: 5
          }
        })
        .spread(function(task, created) {
          return task.setWhens([data.when.afternoon, data.when.evening]);
        });
    })
    .then(function() {
      return models.Task.findAll();
    });
};

var needsBootstrappin = function(models) {
  return models.Child.findAndCountAll()
    .then(function(result) {
      return result.count === 0;
    });
};

var bootstrap = function(models) {
  return needsBootstrappin(models).then(function(doweneed) {
    if (doweneed) {
      return models.Child.bulkCreate([{
          name: "Freja",
          age: 7
        }, {
          name: "Anja",
          age: 5
        }, ])
        .then(function() {
          return models.Child.findAll();
        })
        .then(function(children) {
          return models.When.bulkCreate([{
              id: "morning"
            }, {
              id: "afternoon"
            }, {
              id: "evening"
            }, {
              id: "anytime"
            }])
            .then(function() {
              return models.When.findAll();
            })
            .then(function(whens) {
              mapWhens(whens);
              return createRewards(models);
            })
            .then(function(rewards) {
              return createTasks(models);
            });
        });
    }
    return false;
  });
};

module.exports = bootstrap;
