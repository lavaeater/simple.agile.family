// server.js
// BASE SETUP
// =============================================================================
// call the packages we need
var express = require('express'); // call express
var app = express(); // define our app using express
var bodyParser = require('body-parser');
var _ = require('lodash');
var models = require('./models');
var bootstrap = require('./bootstrap');
var viewmodel = require('./viewmodel');
var moment = require('moment');
var Promise = require('bluebird');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

var port = process.env.PORT || 666; // set our port

var apiRouter = express.Router(); // Router for the more structured api
apiRouter.use(function(request, response, next) {
  console.log(request.url);
  console.log(request.params);
  console.log(request.body);
  next();
});
// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
apiRouter.get('/', function(req, res) {
  res.json({
    message: 'hooray! welcome to our api!'
  });
});

apiRouter.route('/admin')
  .get(function(request, response) {
    viewmodel.getAdminViewModel(models)
      .then(function(adminVm) {
        response.json(adminVm);
      })
      .catch(function(error) {
        response.json(error);
      });
  });

apiRouter.route('/child/name/:name/:when/:typeOfDay/:weekday')
  .get(function(request, response) {

    viewmodel.getChildViewModel(request.params.name, request.params.when, request.params.typeOfDay, request.params.weekday, models)
      .then(function(childVm) {
        response.json(childVm);
      })
      .catch(function(error) {
        response.json(error);
      });
  });

apiRouter.route('/children')
  .get(function(request, response) {
    models.Child.findAll()
      .then(function(children) {
        response.json(_.map(children, function(child) {
          return child.get();
        }));
      });
  });

apiRouter.route('/task/:taskId/:when/:childId/:done')
  .put(function(request, response) {
    console.log('done: ' + request.params.done);
    viewmodel.updateTaskStatusForChild(request.params.taskId, request.params.childId, request.params.when, request.params.done, models)
      .then(function(wut) {
        response.json('ok');
      });
  });

var setWhensOnTask = function(task, whens) {
  var whenIds = _.map(whens, 'id');
  return task.setWhens(whenIds);
};

var setRewardsOnTask = function(task, rewards) {
  var rewardIds = _.map(rewards, 'id');
  return task.setRewards(rewardIds);
};

var updateTaskAssociations = function(task, whens, rewards) {
  return Promise.all(setWhensOnTask(task, whens), setRewardsOnTask(task, rewards))
  .then(function() {
    return task;
  });
};

apiRouter.route('/task')
  .post(function(request, response) {
    if (request.body.id === null) {
      models.Task.create(request.body)
        .then(function(task) {
          return updateTaskAssociations(task, request.body.Whens, request.body.Rewards);
        })
        .then(function(task) {
          response.json(task.get());
        });
    } else {
      models.Task.find({where:{id:request.body.id}})
      .then(function(task) {
        return task.update(request.body);
      })
      .then(function(task) {
        return updateTaskAssociations(task, request.body.Whens, request.body.Rewards);
      })
      .then(function(task) {
        response.json(task.get());
      });
    }
  });

apiRouter.route('/reward')
  .post(function(request, response) {
    if (request.body.id === null) {
      models.Reward.create(request.body)
        .then(function(reward) {
          response.json(reward.get());
        });
    } else {
      models.Reward.find({where:{id:request.body.id}})
      .then(function(reward) {
        return reward.update(request.body);
      })
      .then(function(savedReward) {
        response.json(savedReward);
      });
    }
  });

app.use('/api', apiRouter);

//Static content
app.use('/', express.static('app'));
// START THE SERVER
// =============================================================================

var modelNames = {
  childtask: 'ChildTask',
  child: 'Child',
  task: 'Task',
  timeinday: 'TimeInDay',
  reward: 'Reward'
};

models.sequelize.sync().then(function() {
  bootstrap(models)
    .then(function(tasks) {
      console.log('bootstrapresult' + tasks);
    });

  app.listen(port);
  console.log('Magic happens on port ' + port);
});
