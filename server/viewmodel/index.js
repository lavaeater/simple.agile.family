var _ = require('lodash');
var moment = require('moment'); //Todays date from 24:00 to 23:59
var Promise = require('bluebird');

var vm = {};

var getChildViewModel = function(name, when, typeOfDay, weekday, models) {

  var childViewModel = {};
  var startDate = moment(moment().format('YYYY-MM-DD') + 'T00:00:00').unix();
  var stopDate = moment(moment().format('YYYY-MM-DD') + 'T23:59:00').unix();

  //1. Find by name
  return models.Child.find({
      where: {
        name: name
      }
    })
    .then(function(child) {
      childViewModel.child = child.get();
      return child.getTaskStatuses({
        where: {
          whenId: when,
          doneAt: {
            $between: [startDate, stopDate]
          }
        }
      });
    })
    .then(function(statuses) {
      childViewModel.taskStatuses = _.map(statuses, function(status) {
        return status.get();
      });
      return models.Task.findAll({
        where: {
          minAge: {
            $lte: childViewModel.child.age
          },
          $or: [{
            howOften: 'daily'
          }, {
            howOften: typeOfDay
          }, {
            howOften: weekday
          }]
        },
        include: [{
          model: models.When,
          where: {
            $or: [{
              id: when
            }, {
              id: 'anytime'
            }]
          }
        }, {
          model: models.Reward
        }]
      });
    })
    .then(function(tasks) {
      childViewModel.tasks = _.map(tasks, function(task) {
        return task.get();
      });

      _.forEach(childViewModel.tasks, function(task) {
        _.assign(task, {
          done: _.some(childViewModel.taskStatuses, function(status) {
            return status.TaskId == task.id;
          })
        });
      });
      return models.Reward.findAll({
        include: [{
          model: models.Task
        }]
      });
    })
    .then(function(rewards) {
      childViewModel.rewards = _.map(rewards, function(reward) {
        return reward.get();
      });
      return childViewModel;
    });
};

var updateTaskStatusForChild = function(taskId, childId, whenId, done, models) {
  if (done === 'true') {
    //We gonna need a promise right here now!
    var taskP = models.Task.find({
      where: {
        id: taskId
      }
    });

    var childP = models.Child.find({
      where: {
        id: childId
      }
    });

    var whenP = models.When.find({
      where: {
        id: whenId
      }
    });

    var task;
    var child;
    var when;
    return Promise.all([taskP, childP, whenP])
      .then(function(results) {
        task = results[0];
        child = results[1];
        when = results[2];
        var doneAt = moment().unix();
        return models.TaskStatus.create({
          doneAt: doneAt
        });
      })
      .then(function(taskStatus) {
        return taskStatus.setTask(task);
      })
      .then(function(taskStatus) {
        return taskStatus.setChild(child);
      })
      .then(function(taskStatus) {
        return taskStatus.setWhen(when);
      });
  } else {
    return models.TaskStatus.find({
        where: {
          taskId: taskId,
          whenId: whenId,
          childId: childId
        }
      })
      .then(function(taskStatus) {
        return taskStatus.destroy({
          force: true
        });
      });
  }
};

var listAllTasks = function(models) {
  return models.Task.findAll({
    include: [{
      model: models.When
    }, {
      model: models.Reward
    }]
  });
};

var listAllRewards = function(models) {
  return models.Reward.findAll({
    include: [{model: models.Task}]
  });
};

var listAllWhens = function(models) {
  return models.When.findAll();
};

var getAdminViewModel = function(models) {
  return Promise.all([listAllTasks(models), listAllRewards(models), listAllWhens(models)])
    .then(function(result) {
      return {
        tasks: result[0],
        rewards: result[1],
        whens: result[2]
      };
    });
};

vm.getChildViewModel = getChildViewModel;
vm.updateTaskStatusForChild = updateTaskStatusForChild;
vm.getAdminViewModel = getAdminViewModel;
module.exports = vm;
