"use strict";

var Sequelize = require("sequelize");

var config = {
  pool: {
    max: 1,
    min: 0,
    idle: 10000
  },
  "dialect": "sqlite",
  "storage": "./db.development.sqlite"
};
var sequelize = new Sequelize(config.database, config.username, config.password, config);
var db = {};
var columnConfig = {
  timestamps: false
};

var Child = sequelize.define("Child", {
  name: Sequelize.STRING,
  age: Sequelize.INTEGER
}, columnConfig);

var TaskStatus = sequelize.define("TaskStatus", {
  doneAt: Sequelize.INTEGER
}, columnConfig);

var Task = sequelize.define("Task", {
  title: Sequelize.STRING,
  howOften: Sequelize.ENUM('daily', 'weekday', 'weekend','monday','tuesday','wednesday','thursday','friday','saturday','sunday'),
  minAge: Sequelize.INTEGER
}, columnConfig);

var When = sequelize.define("When", {
  id: {
    type: Sequelize.STRING,
    primaryKey: true
  }
}, columnConfig);

var Reward = sequelize.define("Reward", {
  title: Sequelize.STRING,
  description: Sequelize.STRING
}, columnConfig);

//Associations
Child.hasMany(TaskStatus); //One child has many child taskstatuses
TaskStatus.belongsTo(Task); //Every TaskStatus has exactly one Task (its definition so to speak)
TaskStatus.belongsTo(When); //Every status was DONE at ONE specific time of day -- it's just a string!
TaskStatus.belongsTo(Child); //Roundtrip to child
Task.hasMany(TaskStatus); //One task has many TaskStatuses - is this necessary, useful, possible?
Reward.belongsToMany(Task, {
  through: 'RewardTask'
}); //Every reward can belong to many Tasks and Tasks can belong to many rewards
Task.belongsToMany(Reward, {
  through: 'RewardTask'
});

Task.belongsToMany(When, {
  through: 'TaskWhen'
});

When.belongsToMany(Task, {
  through: 'TaskWhen'
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.Child = Child;
db.Task = Task;
db.TaskStatus = TaskStatus;
db.Reward = Reward;
db.When = When;

module.exports = db;
