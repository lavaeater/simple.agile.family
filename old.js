// ROUTES FOR OUR API
// =============================================================================
var router = express.Router(); // get an instance of the express Router

//Register middleware
router.use(function(request, response, next) {
  console.log(request.url);
  console.log(request.params);
  console.log(request.body);
  next();
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
  res.json({
    message: 'hooray! welcome to our api!'
  });
});

// apiRouter.route('/child/:id')
//   .get(function(request, response) {
//     models.Child.find({
//       where: {
//         id: request.params.id
//       },
//       include: [models.ChildTask]
//     }).then(function(child) {
//       response.json(child);
//     });
//   });
//
// apiRouter.route('/child/:id')
//   .get(function(request, response) {
//     models.Child.find({
//       where: {
//         id: request.params.id
//       },
//       include: [models.ChildTask]
//     }).then(function(child) {
//       response.json(child);
//     });
//   });
//
// //'/'+ childId + '/' + when + '/' + date.format('YYYY-mm-dd')
// apiRouter.route('/childtask/:childId/:date')
//   .get(function(request, response) {
//     getChildTasksForChildId(request.params.childId, request.params.date, response);
//   });
//
// var getChildTasksForChildId = function(childId, date, response) {
//   //date is a string: YYYY-MM-DD, create two dates:
//
//   var startDate = moment(moment.unix(date).format('YYYY-MM-DD') + 'T00:00:00').unix();
//   var stopDate = moment(moment.unix(date).format('YYYY-MM-DD') + 'T23:59:00').unix();
//
//   models.ChildTask.findAll({
//     where: {
//       childId: childId,
//       forDate: {
//         $gte: startDate,
//         $lte: stopDate
//       }
//     }
//   }).then(function(childTasks) {
//     response.json(childTasks);
//   });
// };

//
// apiRouter.route('/childtask')
//   .post(function(request, response) {
//     models.ChildTask.bulkCreate(request.body).then(function() {
//       var paramTask = _.first(request.body);
//
//       getChildTasksForChildId(paramTask.childId, paramTask.forDate, response);
//     });
//   });
//
// //Generic ROUTES
// router.route('/:model')
//   .get(function(request, response) {
//     var modelName = modelNames[request.params.model];
//
//     models[modelName].findAll().then(function(model) {
//       response.json(model);
//     });
//   })
//   .post(function(request, response) {
//     var modelName = modelNames[request.params.model];
//     models[modelName].create(request.body).then(function(model) {
//       response.json(model);
//     });
//   });
//
// router.route('/:model/:id')
//   .get(function(request, response) {
//     var modelName = modelNames[request.params.model];
//     models[modelName].findById(request.params.id).then(function(model) {
//       response.json(model);
//     });
//   })
//   .delete(function(request, response) {
//     var modelName = modelNames[request.params.model];
//     models[modelName].findById(request.params.id).then(function(model) {
//       model.destroy().then(response.json({
//         model: modelName,
//         id: model.id,
//         status: 'deleted'
//       }));
//     });
//   })
//   .put(function(request, response) {
//     var modelName = modelNames[request.params.model];
//     models[modelName].findById(request.params.id).then(function(model) {
//       model.update(request.body).then(function() {
//         response.json({
//           model: modelName,
//           id: model.id,
//           status: 'updated'
//         });
//       });
//     });
//   }); //Add more here
//
// router.route('/:model/name/:name')
//   .get(function(request, response) {
//     var modelName = modelNames[request.params.model];
//     if (modelName === 'Child') {
//       models[modelName].find({
//         where: {
//           name: request.params.name
//         }
//       }).then(function(model) {
//         response.json(model);
//       });
//     }
//   });
//
// // REGISTER OUR ROUTES -------------------------------
// // all of our routes will be prefixed with /api
// app.use('/entities', router);
